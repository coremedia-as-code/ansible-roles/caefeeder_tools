# CoreMedia - command line tool for caefeeder

## usage

```
$ cd /opt/coremedia/caefeeder-live-tools
$ bin/cm version
1904.2 - 14.06.2019 - 03:27:12

$ cd /opt/coremedia/caefeeder-preview-tools
$ bin/cm version
1904.2 - 14.06.2019 - 03:27:12

```

## configure

with `host_vars`

```
application_database:
  host: mysql.cm.local
  name: cm_caefeeder
  user: cm_caefeeder
  password: cm_caefeeder
```

or with playbook

```
- role: caefeeder_tools
  vars:
    - coremedia_application_name: caefeeder-live-tools
    - application_database:
        name: cm_caefeeder
        user: cm_caefeeder
        password: cm_caefeeder
```

**Both ways are valid and can be combined with each other.**
